# frozen_string_literal: true

require 'nokogiri'
require 'open-uri'
require 'cgi'
require 'fileutils'
require 'json'
require 'optparse'

$options = {
  host: 'conf.fsfe.org'
}
OptionParser.new do |opts|
  opts.banner = "Usage: download_client.rb [options]"

  opts.on('-h', '--host HOST', 'BigBlueButton hostname') { |v| $options[:host] = v }
  opts.on('-i', '--id ID', 'BigBlueButton meeting ID') { |v| $options[:id] = v }

end.parse!

FileUtils.mkdir_p "presentation/#{$options[:id]}/video"
FileUtils.mkdir_p "presentation/#{$options[:id]}/deskshare"
FileUtils.mkdir_p "presentation/#{$options[:id]}/presentation"
Dir.chdir "presentation/#{$options[:id]}"

def download(file)
  # Format: "https://hostname/presentation/meetingID/#{file}"

  path = "https://#{$options[:host]}/presentation/#{$options[:id]}/#{file}"

  puts "Downloading #{path}"

  File.open(file, 'wb') do |get|
    get << URI.parse(path.to_s).open(&:read)
  end
end

# Downloads the recording's assets
# Whiteboard: 'shapes.svg', 'cursor.xml', 'panzooms.xml', 'presentation_text.json', 'captions.json', 'metadata.xml'
# Video: 'video/webcams.mp4', 'deskshare/deskshare.mp4'
# Chat: 'slides_new.xml'

['shapes.svg', 'cursor.xml', 'panzooms.xml', 'presentation_text.json', 'captions.json', 'metadata.xml',
 'video/webcams.webm', 'deskshare/deskshare.webm', 'slides_new.xml'].each do |get|
  download(get)
end

# Opens shapes.svg
@doc = Nokogiri::XML(File.open('shapes.svg'))

slides = @doc.xpath('//xmlns:image', 'xmlns' => 'http://www.w3.org/2000/svg', 'xlink' => 'http://www.w3.org/1999/xlink')

# Download all captions
json = JSON.parse(File.read('captions.json'))

(0..json.length - 1).each do |i|
  download "caption_#{json[i]['locale']}.vtt"
end

# Downloads each slide
slides.each do |img|
  path = File.dirname(img.attr('xlink:href'))

  # Creates folder structure if it's not yet present
  FileUtils.mkdir_p(path) unless File.directory?(path)

  download(img.attr('xlink:href'))
end
