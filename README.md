
# BigBlueButton Exporter

Client-side version of the script that enables users to download BigBlueButton
2.3 recordings as a single video file.

A fork of the `client-side` branch of
https://github.com/danielpetri1/bbb-recording-exporter/

## Usage

First of all, you need to know the ID of the BigBlueButton meeting you have
recorded. Let's say, the BBB presentation link is
`https://conf.fsfe.org/playback/presentation/2.3/876393061e41e72de6bba58123451ee5a970d503-1649332654305`.
In this case, `876393061e41e72de6bba58123451ee5a970d503-1649332654305` is the ID
you need to provide.

Also, you need to have `ruby` installed. There are also some ruby libraries
necessary that may not be available on your system by default. Typically, they
should be available in your distribution's repository, e.g. `ruby-loofah`.

Then, it is recommended to clone this repository and work within this directory.

### 1. Download all required parts

With the first command, you download all required parts:

`ruby download_client.rb -i 876393061e41e72de6bba58123451ee5a970d503-1649332654305`

By default, the recording is searched on the FSFE's BBB instance. If you want to
download a recording from another instance, provide e.g. `-h
other.bbb-host.com`.

### 2. Export to a single video file

The second command compiles everything into one file. Depending on the recording
length and your computing power, this may take a couple of minutes:

`ruby export_presentation.rb -i 876393061e41e72de6bba58123451ee5a970d503-1649332654305`

If this is done, you will find the final file in
`presentation/876393061e41e72de6bba58123451ee5a970d503-1649332654305/`.

## Customisation

The `export_presentation.rb` file contains a number of variables that can
configure the actual display of the file, e.g. the size or whether to show the
chat. This has to be done manually for now.
